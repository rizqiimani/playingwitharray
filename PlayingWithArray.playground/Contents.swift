import UIKit

//Buat variable dalam bentuk Array 2, 3, 10, 8, 7, 3, 1, 0. Cari nilai terbesar, terkecil, diurutkan dari A to Z dan Z to A. Cari duplikatnya.
//Hint: Pakai perulangan dan if

let arraydata = [2, 3, 10, 8, 7, 3, 25, 1 , 0, -8, -5]
var maxvalue = 0
var minvalue = 0
var dupvalue = 0

//kode mencari nilai terbesar
for (index, value) in  arraydata.enumerated() {
    if index == 0 { //untuk memastikan dalam array ada isinya
        maxvalue = value
    } else if maxvalue < value{
        maxvalue = value
    }
}
print(maxvalue)

//kode mencari nilai terkecil
for (index1, value1) in arraydata.enumerated() {
    if index1 == 0 { //untuk memastikan dalam array ada isinya
        minvalue = value1
    } else if minvalue > value1 {
        minvalue = value1
    }
}
print(minvalue)

//fungsi mencari duplikat
